from django.urls import path, include
from .views import index,registro,login_view,editar_perfil,ejercicios, triceps,espalda,piernas, cerrar_sesion,cambiar_contraseña,adm_usuarios
from .views import eliminar_usuario,adm_posts,eliminar_post, alimentacion, rutinas,editar_post
from django.conf.urls.static import static
from django.conf import settings
from posts.views import perfil,subir_post,post



urlpatterns = [
    path('', index,name='index'),
    path('login/',login_view, name='login'),
    path('registro/',registro, name='registro'),
    path('editar_perfil/',editar_perfil, name='editar_perfil'),
    path('ejercicios/',ejercicios, name='ejercicios'),
    path('triceps/',triceps,name='pechoTriceps'),
    path('espalda/',espalda,name='espaldaBiceps'),
    path('piernas/', piernas,name='piernas'),
    path('cerrar_sesion',cerrar_sesion,name="cerrar_sesion"),
    path('profile/cambiar_clave/',cambiar_contraseña,name='cambiar_clave'),
    path('perfil/',perfil,name='perfil'),
    path('subir_post/',subir_post,name='subir_post'),
    path('adm_usuarios',adm_usuarios, name='adm_usuarios'),
    path('eliminar_usuario/<int:id>/usuario/',eliminar_usuario,name="eliminar_usuario"),
    path('adm_publicaciones/<int:id>',adm_posts, name='adm_posts'),
    path('eliminar_post/<int:id>',eliminar_post,name='eliminar_post'),
    path('alimentacion/',alimentacion,name='alimentacion'),
    path('rutinas/',rutinas,name='rutinas'),
    path('post/<int:id>/',post,name='post'),
    path('editar_post/<int:id>/', editar_post, name='editar_post'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
